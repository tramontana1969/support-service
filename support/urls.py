from django.contrib import admin
from django.urls import include, path

from rest_framework import routers
from rest_framework_simplejwt.views import TokenRefreshView

from tickets.urls import router as ticket_router
from users.urls import router as user_router
from users.views import MyTokenObtainPairView

router = routers.DefaultRouter()
router.registry.extend(user_router.registry)
router.registry.extend(ticket_router.registry)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include(router.urls)),
    path('api_auth', include('rest_framework.urls')),
    path('api/token/', MyTokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),

]
