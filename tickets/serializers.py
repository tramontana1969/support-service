from rest_framework import serializers

from tickets.models import Message, Ticket
from tickets.service import update_answer_field
from tickets.tasks import abort_task, change_status_task


class TicketSerializer(serializers.ModelSerializer):
    """
    While users in 'Customers' user group should have an opportunity to write
    the topic and the text of the 'Ticket', users in 'Supports' user group
    should be able to change only status of the 'Ticket'. For implementing of
    such kind separated permission rights is used "__init__()" method.

    Serializers fields
    ------------------
    'published_date':
        Creation date don't need to set manually, so 'ReadOnlyField() is used'.
    'author':
        'StringRelatedField()' is used to view author's username.
    'response':
        For implementation 'The Browsable API' is used 'HyperlinkedRelatedField()'
        to allow opening and viewing response messages.
    'url':
        For implementation the opportunity to get 'Ticket' object fastly via
        'The Browsable API' is used 'HyperlinkedRelatedField()'.
    'topic':
        'ReadOnlyField()' gives possibility only to read topic of the 'Ticket' for
         'Supports'.
    'text':
        'ReadOnlyField()' gives possibility only to read text of the 'Ticket' for
        'Supports'.
    """

    published_date = serializers.ReadOnlyField()
    author = serializers.StringRelatedField(read_only=True)
    response = serializers.HyperlinkedRelatedField(many=True, read_only=True, view_name='responses-detail')
    url = serializers.HyperlinkedIdentityField(view_name='tickets-detail')
    topic = serializers.ReadOnlyField()
    text = serializers.ReadOnlyField()

    class Meta(object):
        model = Ticket
        fields = ('id', 'url', 'author', 'topic', 'text', 'status', 'published_date', 'response')

    def __init__(self, *args, **kwargs):
        """
        Changes permission rights to have an ability fill 'topic' and 'text' fields
        for 'Customers' and prohibit change ticket's 'status'.
        """

        super(TicketSerializer, self).__init__(*args, **kwargs)
        if self.context['request'].user.group == 'Customers':
            self.fields['status'] = serializers.ReadOnlyField()
            self.fields['topic'] = serializers.CharField(max_length=30)
            self.fields['text'] = serializers.CharField()

    def create(self, validated_data):
        """
        Creates new ticket.

        During the creating ticket runs 'Celery' task for changing ticket's status
        automatically after a set time.
        Method 'save()' is called twice. The first one calls for saving all ticket's
        data. And the second one is called after running asynchronous task to save
        task's id.
        """

        ticket = Ticket(
            author=self.context['request'].user,
            topic=validated_data['topic'],
            text=validated_data['text'],
        )
        ticket.save()
        task = change_status_task.apply_async((ticket,), countdown=180)
        ticket.task_id = task.id
        ticket.save()
        return ticket

    def update(self, instance, validated_data):
        """
        Updates an existing ticket.

        Function 'abort_task()' takes 'task_id' and discards asynchronous task.
        """

        instance.topic = validated_data.get('topic', instance.topic)
        instance.text = validated_data.get('text', instance.text)
        instance.status = validated_data.get('status', instance.status)
        abort_task(instance.task_id)
        instance.save()
        return instance


class MessageSerializer(serializers.ModelSerializer):
    """
    Class 'MessageSerializer' provides serialization of the 'Message' object.

    Serializers fields
    ------------------
    'url':
        For implementation the opportunity to get 'Ticket' object fastly via
        'The Browsable API' is used 'HyperlinkedRelatedField()'.
    'author', 'recipient':
        'StringRelatedField()' is used to represent the target of the relationship
        with tickets author and to view author's and recipient's usernames.
    'ticket', 'parent', 'answer':
        For implementation 'The Browsable API' is used 'HyperlinkedRelatedField()'.
    date':
        Creation date don't need to set manually, so 'ReadOnlyField() is used'.
    """

    url = serializers.HyperlinkedIdentityField(view_name='responses-detail')
    author = serializers.StringRelatedField(read_only=True)
    recipient = serializers.StringRelatedField(read_only=True)
    ticket = serializers.HyperlinkedRelatedField(queryset=Ticket.objects.all(), view_name='tickets-detail')
    parent = serializers.HyperlinkedRelatedField(
        allow_null=True, queryset=Message.objects.all(), view_name='responses-detail'
    )
    date = serializers.ReadOnlyField()
    answer = serializers.HyperlinkedRelatedField(allow_null=True, view_name='responses-detail', read_only=True)

    class Meta(object):
        model = Message
        fields = ('id', 'url', 'ticket', 'author', 'recipient', 'ticket', 'text', 'parent', 'date', 'answer')

    def create(self, validated_data):
        """
        Creates new message.

        When support user responds to the ticket, the recipient is ticket's author.
        And when user (support or customer) responds to the message, the recipient
        is parent message's author.
        Also when user responds to the message, parent message's field 'url' should
        be updated to show answer message. This opportunity is achieved by using
        'update_answer_field()' function.
        """

        if validated_data['parent'] is None:
            recipient = validated_data['ticket'].author
        else:
            recipient = validated_data['parent'].author

        message = Message(
            ticket=validated_data['ticket'],
            author=self.context['request'].user,
            text=validated_data['text'],
            recipient=recipient,
            parent=validated_data['parent'],
        )
        message.save()

        if validated_data['parent'] is not None:
            update_answer_field(self, message, validated_data)

        return message

    def update(self, instance, validated_data):
        instance.answer = validated_data.get('answer', instance.answer)
        return instance
