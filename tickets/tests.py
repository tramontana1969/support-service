from django.contrib.auth import get_user_model

import pytest

from tickets.models import Ticket

User = get_user_model()


@pytest.mark.django_db
def test_ticket():
    ticket = Ticket.objects.create(
        author=User.objects.create(
            username='John',
            email='John@john.com',
            password='12345'
        ),
        topic='my topic',
        text='my text'
    )
    return ticket


@pytest.mark.django_db
def test_update_ticket():
    ticket = Ticket.objects.create(
        author=User.objects.create(
            username='John',
            email='John@john.com',
            password='12345'
        ),
        topic='my topic',
    )
    ticket.topic = 'another topic'
    ticket.save()
    assert ticket.topic == 'another topic'
