from django.db import models

from support import settings
from tickets.choices import TicketStatus


class Ticket(models.Model):
    """
    Class 'Ticket' represents a ticket model.

    Fields
    ------
    'author':
        Ticket's author.
    'topic':
        Ticket's topic. Used for short description of ticket's content.
    'text':
        Ticket text. Used for detailed description of ticket's content.
    'published_date':
        Date of ticket's creation.
    'status':
        Shows ticket's status. By default, when ticket is created, the status is 'Active'.
        After a certain period of time status changes to 'Frozen'. For example were
        taken 3 minutes. But if support user updates status, the chosen will be selected.
    'task_id':
        During the ticket creation, when 'create()' method runs, starts an asynchronous
        task that changes ticket status automatically. When support user updates ticket
        status, 'task_ud' is used to determine 'id' of asynchronous task in 'update()'
        method and discard the task.
    """

    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='ticket')
    topic = models.CharField(max_length=30)
    text = models.TextField()
    published_date = models.DateTimeField(auto_now_add=True)
    status = models.CharField(max_length=255, choices=TicketStatus.choices(), default='Active')
    task_id = models.TextField()

    def __str__(self):
        return self.topic


class Message(models.Model):
    """
    Class 'Message' represents message model.

    Fields
    ------
    'author':
        Message author.
    'recipient':
        Recipient of the message.
    'ticket':
        Ticket, which answers are written.
    'parent':
        Parent allows to determine a message, to which answer is written.
    'answer':
        Answer allows to determine a message, which was used to reply.
    'text':
        Message text.
    'date':
        Date of message creation
    """

    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='response')
    recipient = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='message')
    ticket = models.ForeignKey(Ticket, on_delete=models.CASCADE, related_name='response')
    parent = models.ForeignKey('self', blank=True, null=True, on_delete=models.CASCADE, related_name='response')
    answer = models.ForeignKey('self', blank=True, null=True, on_delete=models.CASCADE, related_name='Message')
    text = models.TextField()
    date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.text
