def update_answer_field(self, obj, validated_data):
    """Updates message's field 'answer', when some message responds to other."""
    parent_message = obj.parent
    parent_message.answer = obj
    self.update(parent_message, validated_data)
    parent_message.save()
    return parent_message


def change_ticket_status(obj):
    """Changes ticket's status."""
    status = 'Frozen'
    obj.status = status
    obj.save()
    return obj


def ticket_queryset_filter(self, obj):
    """
    Supports are allowed to view all tickets, while customers are allowed to
    view only their own tickets.
    """
    user = self.request.user
    if user.group == 'Supports':
        return obj.objects.all()
    else:
        return obj.objects.filter(author=user)


def message_queryset_filter(self, obj):
    """Allows to view only sent and received messages, which belong to user."""
    user = self.request.user
    return obj.objects.filter(recipient=user) | obj.objects.filter(author=user)
