from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import permissions, viewsets

from permissions import IsOwnerOrRearOnly
from tickets.models import Message, Ticket
from tickets.serializers import MessageSerializer, TicketSerializer
from tickets.service import message_queryset_filter, ticket_queryset_filter


class ViewTickets(viewsets.ModelViewSet):

    serializer_class = TicketSerializer
    permission_classes = [permissions.IsAuthenticated]
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['status']

    def get_queryset(self):
        return ticket_queryset_filter(self, Ticket)


class MessageViewSet(viewsets.ModelViewSet):
    serializer_class = MessageSerializer
    permission_classes = [permissions.IsAuthenticated, IsOwnerOrRearOnly]

    def get_queryset(self):
        return message_queryset_filter(self, Message)
