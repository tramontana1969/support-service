from rest_framework import routers

from tickets.views import MessageViewSet, ViewTickets

router = routers.DefaultRouter()
router.register(r'tickets', ViewTickets, basename='tickets')
router.register(r'responses', MessageViewSet, basename='responses')
