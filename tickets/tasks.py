from celery.contrib.abortable import AbortableAsyncResult, AbortableTask

from support.celery import app
from tickets.service import change_ticket_status


@app.task(blind=True, base=AbortableTask)
def change_status_task(obj):
    """Starts asynchronous task to change ticket's status automatically."""
    change_ticket_status(obj)


@app.task(blind=True)
def abort_task(foo_id):
    """Discards asynchronous task when ticket's status updates manually."""
    task = AbortableAsyncResult(foo_id)
    task.revoke(terminate=True)
