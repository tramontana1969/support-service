from enum import Enum


class TicketStatus(str, Enum):
    Active = 'Active'
    Resolved = 'Resolved'
    Frozen = 'Frozen'

    @classmethod
    def choices(cls):
        return [(i, i.value) for i in cls]

    def __str__(self):
        return self.name
