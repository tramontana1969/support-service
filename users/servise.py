def user_queryset_filter(self, obj):
    """
    While simple users are allowed to view  and update only their own account page,
    admin user is allowed to all pages for implementing changing users status.
    """
    user = self.request.user
    if user.is_staff:
        return obj.objects.all()
    else:
        return obj.objects.filter(id=user.id)
