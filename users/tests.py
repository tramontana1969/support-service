from django.contrib.auth import get_user_model

import pytest
import requests

User = get_user_model()


@pytest.mark.django_db
def test_user():
    user = User.objects.create(
        username='John',
        email='John@john.com',
        password='12345'
    )
    assert user.username == 'John'
    assert user.email == 'John@john.com'
    assert user.password == '12345'


@pytest.mark.django_db
def test_update_user():
    user = User.objects.create(
        username='John',
        email='John@john.com',
        password='12345'
    )
    user.username = 'Victor'
    user.save()
    assert user.username == 'Victor'


def test_get_all_users():
    response = requests.get('http://0.0.0.0:8000/users/')
    assert response.status_code == 200


def test_add_user(live_server):
    data = {
        'username': 'Alex',
        'email': 'alex@alex.com',
        'first_name': 'Sasha',
        'last_name': 'Ivanov',
        'password': '12345'
    }
    response = requests.post('http://127.0.0.1:8000/users/', data)
    assert response.status_code == 201


def test_get_token():
    data = {
        'username': 'Alex',
        'password': '123'
    }
    response = requests.post('http://0.0.0.0:8000/api/token/', data)
    assert response.status_code == 401
