from django.contrib.auth import get_user_model
from django.contrib.auth.hashers import make_password

from rest_framework import serializers
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer

from users.choices import UserGroup


class UserSerializer(serializers.ModelSerializer):
    """
    Admin user have an opportunity to change user group and other users should
    should be allowed to update their data.
    For implementing of such kind separated permission rights is used "__init__()"
    method.
    """

    date_joined = serializers.ReadOnlyField()
    ticket = serializers.HyperlinkedRelatedField(many=True, read_only=True, view_name='tickets-detail')
    password = serializers.CharField(style={'input_type': 'password'}, write_only=True)
    url = serializers.HyperlinkedIdentityField(view_name='users-detail')

    class Meta(object):
        model = get_user_model()
        fields = ('id', 'email', 'url', 'username', 'first_name', 'last_name', 'ticket', 'date_joined', 'password')

    def __init__(self, *args, **kwargs):
        super(UserSerializer, self).__init__(*args, **kwargs)
        if self.context['request'].user.is_staff:
            self.fields['group'] = serializers.ChoiceField(choices=UserGroup)
            self.fields['email'] = serializers.ReadOnlyField()
            self.fields['username'] = serializers.ReadOnlyField()
            self.fields['first_name'] = serializers.ReadOnlyField()
            self.fields['last_name'] = serializers.ReadOnlyField()
            del (self.fields['password'])

    def create(self, validated_data):
        user = get_user_model()(
            username=validated_data['username'],
            email=validated_data['email'],
            first_name=validated_data['first_name'],
            last_name=validated_data['last_name'],
            password=make_password(self.context['request'].data['password']),
        )
        user.save()
        return user

    def update(self, instance, validated_data):
        if self.context['request'].user.is_staff:
            instance.group = validated_data.get('group', instance.group)
        else:
            instance.username = validated_data.get('username', instance.username)
            instance.email = validated_data.get('email', instance.email)
            instance.first_name = validated_data.get('first_name', instance.first_name)
            instance.last_name = validated_data.get('last_name', instance.last_name)
            instance.password = make_password(self.context['request'].data['password'])
        instance.save()
        return instance


class MyTokenObtainPairSerializer(TokenObtainPairSerializer):
    """
    class 'MyTokenObtainPairSerializer' allows to get access and refresh tokens.
    """

    @classmethod
    def get_token(cls, user):
        token = super().get_token(user)
        token['username'] = user.username
        token['email'] = user.email
        token['group'] = user.group
        return token
