from enum import Enum


class UserGroup(str, Enum):
    Customers = 'Customers'
    Supports = 'Supports'

    @classmethod
    def choices(cls):
        return [(i, i.value) for i in cls]

    def __str__(self):
        return self.name
