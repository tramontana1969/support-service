from __future__ import unicode_literals

from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from django.contrib.auth.models import UserManager as AbstractUserManager
from django.db import models, transaction
from django.utils import timezone

from users.choices import UserGroup


class UserManager(AbstractUserManager):
    """
    Class 'UserManager' assigns main methods for creating user, such as creating
    simple user and superuser.
    """

    def _create_user(self, username, email, password, **extra_fields):
        if not username:
            raise ValueError('Require username')
        with transaction.atomic():
            user = self.model(email=email, username=username, **extra_fields)
            user.password = make_password(password)
            user.save(using=self.db)
            return user

        def create_user(self, username, email, password, **extra_fields):
            extra_fields.setdefault('is_staff', False)
            extra_fields.setdefault('is_superuser', False)
            return self._create_user(username, email, password, **extra_fields)

        def create_superuser(self, username, email, password, **extra_fields):
            extra_fields.setdefault('is_staff', True),
            extra_fields.setdefault('is_superuser', True)
            return self._create_user(username, email, password=password, **extra_fields)


class User(AbstractBaseUser, PermissionsMixin):
    """
    Class 'User' represents User model.

    Users are divided into two groups: 'Supports' and 'Customers'. When user is
    created, by default he belongs to Customers. For implementing an opportunity
    to set default group during the creating user field 'group' is used.
    """

    username = models.CharField(max_length=25, unique=True)
    email = models.EmailField(max_length=25, unique=True)
    first_name = models.CharField(max_length=25, blank=True)
    last_name = models.CharField(max_length=25, blank=True)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    date_joined = models.DateTimeField(default=timezone.now)
    group = models.CharField(max_length=255, choices=UserGroup.choices(), default='Customers')

    objects = UserManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']

    def save(self, *args, **kwargs):
        super(User, self).save(*args, **kwargs)
        return self
